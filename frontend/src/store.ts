import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import { AuthActions } from "./redux/auth/actions";
import { authReducer, AuthState } from "./redux/auth/reducer";
import { createBrowserHistory } from 'history';
import { createReduxHistoryContext, RouterState } from "redux-first-history";
import thunk, { ThunkDispatch } from "redux-thunk";
import LogRocket from 'logrocket';
import { todoListReducer, TodoListState } from "./redux/todoList/reducer";
import { TodoListActions } from "./redux/todoList/actions";

const { createReduxHistory, routerMiddleware, routerReducer } = createReduxHistoryContext({
    history: createBrowserHistory(),
});
export interface RootState {
    auth: AuthState
    router: RouterState
    todoList: TodoListState
}

export type RootAction = AuthActions | TodoListActions;

export type RootThunkDispatch = ThunkDispatch<RootState, null, RootAction>;

const reducer = combineReducers<RootState>({
    auth: authReducer,
    router: routerReducer,
    todoList: todoListReducer
});

declare global {
    /* tslint:disable:interface-name */
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
    }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore<RootState, RootAction, {}, {}>(
    reducer,
    composeEnhancers(
        applyMiddleware(thunk),
        applyMiddleware(routerMiddleware),
        applyMiddleware(LogRocket.reduxMiddleware()),
    )
);

export const history = createReduxHistory(store);