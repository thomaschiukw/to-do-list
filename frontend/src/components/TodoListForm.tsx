import React from 'react';
import "./TodoListForm.css"
import { Controller, SubmitHandler, useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { addTodoList } from '../redux/todoList/thunk';
import { Button, TextField } from '@mui/material';

export interface TodoListFormValues {
    name: string;
}

function TodoListForm() {
    const dispatch = useDispatch();

    const { control, setValue, handleSubmit, formState: { errors } } = useForm<TodoListFormValues>({
        defaultValues: {
            name: '',
        }
    });
    const onSubmit: SubmitHandler<TodoListFormValues> = data => {
        dispatch(addTodoList(data));
        setValue('name', '');
    };

    return (
        <div className="list-form">
            <form onSubmit={handleSubmit(onSubmit)}>
                <Controller
                    name="name"
                    control={control}
                    rules={{ required: "List name is required" }}
                    render={({ field }) => <TextField
                        {...field}
                        error={errors.name && true}
                        className="list-textfield"
                        id="name"
                        label="Create a new list"
                        variant="outlined"
                        size="small"
                        helperText={errors.name && `${errors.name.message}`}
                    />}
                />
                <Button
                    className="list-button"
                    type="submit"
                    variant="contained"
                >
                    Create
                </Button>
            </form>
        </div>
    )
}

export default TodoListForm
