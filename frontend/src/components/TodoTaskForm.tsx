import React from 'react';
import "./TodoTaskForm.css"
import { addTodoTask } from '../redux/todoList/thunk';
import { useDispatch } from 'react-redux';
import { Controller, SubmitHandler, useForm } from 'react-hook-form';
import { Fab, ListItem, ListItemIcon, ListItemText, TextField } from '@mui/material';
import { add } from "date-fns";
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DateTimePicker from '@mui/lab/DateTimePicker';
import AddIcon from '@mui/icons-material/Add';

interface Props {
    id: number
}

export interface TodoTaskFormValues {
    id: number;
    name: string;
    description?: string;
    deadline: Date;
}

function TodoTaskForm(props: Props) {
    const dispatch = useDispatch();
    const { control, setValue, handleSubmit, formState: { errors } } = useForm<TodoTaskFormValues>({
        defaultValues: {
            name: '',
            description: '',
            deadline: add(new Date(), { days: 1 })
        }
    });

    const onSubmit: SubmitHandler<TodoTaskFormValues> = data => {
        dispatch(addTodoTask(data));
        setValue('name', '');
        setValue('description', '');
        setValue('deadline', add(new Date(), { days: 1 }));
    };

    return (
        <ListItem>
            <ListItemText
                primary={
                    <LocalizationProvider dateAdapter={AdapterDateFns}>
                        <div>
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <Controller
                                    name="name"
                                    control={control}
                                    rules={{ required: "Name is required" }}
                                    render={({ field }) => <TextField
                                        {...field}
                                        className="task-form-elem"
                                        error={errors.name && true}
                                        id="name"
                                        label="Name"
                                        variant="outlined"
                                        size="small"
                                        helperText={errors.name && `${errors.name.message}`}
                                    />}
                                />

                                <Controller
                                    name="description"
                                    control={control}
                                    render={({ field }) => <TextField
                                        {...field}
                                        className="task-form-elem"
                                        id="description"
                                        label="Description"
                                        variant="outlined"
                                        size="small"
                                    />}
                                />
                                <Controller
                                    name="deadline"
                                    control={control}
                                    render={({ field: { onChange, value } }) => <DateTimePicker
                                        label="Deadline"
                                        value={value}
                                        onChange={onChange}
                                        renderInput={(params) => <TextField
                                            {...params}
                                            size="small"
                                            className="task-form-elem"
                                        />}
                                    />}
                                />

                                <ListItemIcon>
                                    <Fab
                                        className="task-form-elem"
                                        size="small"
                                        color="primary"
                                        aria-label="add"
                                        onClick={() => { setValue("id", props.id) }}
                                        type="submit"
                                    >
                                        <AddIcon />
                                    </Fab>
                                </ListItemIcon>
                            </form>
                        </div>
                    </LocalizationProvider>
                }
            />
        </ListItem>
    )
}

export default TodoTaskForm;