import React, { useEffect, useState } from 'react';
import { Snackbar, Alert } from '@mui/material';
import { socket } from "../App";
import { fetchTodoList, fetchTodoTask } from '../redux/todoList/thunk';
import { useDispatch } from 'react-redux';

function Snacker() {
    const dispatch = useDispatch();
    const [isListCreated, setIsListCreated] = useState(false);
    const [isTaskCreated, setIsTaskCreated] = useState(false);
    const [isTaskEdited, setIsTaskEdited] = useState(false);
    const [isTaskMoved, setIsTaskMoved] = useState(false);
    const [isTasksMoved, setIsTasksMoved] = useState(false);
    const [isTasksCompleted, setIsTasksCompleted] = useState(false);
    const [isListRemoved, setIsListRemoved] = useState(false);
    const [isTaskRemoved, setIsTaskRemoved] = useState(false);
    const [isTasksRemoved, setIsTasksRemoved] = useState(false);

    const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }

        setIsListCreated(false);
        setIsTaskCreated(false);
        setIsTaskEdited(false);
        setIsTaskMoved(false);
        setIsTasksMoved(false);
        setIsTasksCompleted(false);
        setIsListRemoved(false);
        setIsTaskRemoved(false);
        setIsTasksRemoved(false);
    };

    useEffect(() => {
        socket.on("list-created", () => {
            dispatch(fetchTodoList())
            setIsListCreated(!isListCreated)
        });
        socket.on("task-created", () => {
            dispatch(fetchTodoTask())
            setIsTaskCreated(!isTaskCreated)
        });
        socket.on("task-edited", () => {
            dispatch(fetchTodoTask())
            setIsTaskEdited(!isTaskEdited)
        });
        socket.on("task-moved", () => {
            dispatch(fetchTodoTask())
            setIsTaskMoved(!isTaskMoved)
        });
        socket.on("tasks-moved", () => {
            dispatch(fetchTodoTask())
            setIsTasksMoved(!isTasksMoved)
        });
        socket.on("task-completed", () => {
            console.log("Task completed!");
            dispatch(fetchTodoTask())
            setIsTasksCompleted(!isTasksCompleted)
        });
        socket.on("list-removed", () => {
            dispatch(fetchTodoList())
            setIsListRemoved(!isListRemoved)
        });
        socket.on("task-removed", () => {
            dispatch(fetchTodoTask())
            setIsTaskRemoved(!isTaskRemoved)

        });
        socket.on("tasks-removed", () => {
            dispatch(fetchTodoTask())
            setIsTasksRemoved(!isTasksRemoved)
        });

        return () => {
            socket.off("list-created", () => { dispatch(fetchTodoList()) });
            socket.off("task-created", () => { dispatch(fetchTodoTask()) });
            socket.off("task-edited", () => { dispatch(fetchTodoTask()) });
            socket.off("task-moved", () => { dispatch(fetchTodoTask()) });
            socket.off("tasks-moved", () => { dispatch(fetchTodoTask()) });
            socket.off("task-completed", () => { dispatch(fetchTodoTask()) });
            socket.off("list-removed", () => { dispatch(fetchTodoList()) });
            socket.off("task-removed", () => { dispatch(fetchTodoTask()) });
            socket.off("tasks-removed", () => { dispatch(fetchTodoTask()) });
        }
    }, [dispatch, isListCreated, isListRemoved, isTaskCreated, isTaskEdited, isTaskMoved, isTaskRemoved, isTasksCompleted, isTasksMoved, isTasksRemoved])


    return (
        <div>
            <Snackbar open={isListCreated} autoHideDuration={3000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                    New list created successfully!
                </Alert>
            </Snackbar>

            <Snackbar open={isTaskCreated} autoHideDuration={3000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                    New Task created successfully!
                </Alert>
            </Snackbar>

            <Snackbar open={isTaskEdited} autoHideDuration={3000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                    Task edited successfully!
                </Alert>
            </Snackbar>


            <Snackbar open={isTaskMoved} autoHideDuration={3000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                    Task moved successfully!
                </Alert>
            </Snackbar>

            <Snackbar open={isTasksMoved} autoHideDuration={3000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                    Selected tasks moved successfully!
                </Alert>
            </Snackbar>

            <Snackbar open={isTasksCompleted} autoHideDuration={3000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                    Task completed! Congrats!
                </Alert>
            </Snackbar>

            <Snackbar open={isListRemoved} autoHideDuration={3000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                    List removed successfully!
                </Alert>
            </Snackbar>

            <Snackbar open={isTaskRemoved} autoHideDuration={3000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                    Task removed successfully!
                </Alert>
            </Snackbar>

            <Snackbar open={isTasksRemoved} autoHideDuration={3000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                    Selected tasks removed successfully!
                </Alert>
            </Snackbar>
        </div>
    )
}

export default Snacker
