import React, { useState } from 'react'
import './TodoListsDisplay.css';
import TodoTasksDisplay from './TodoTasksDisplay';
import TodoTaskForm from './TodoTaskForm';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../store';
import { List as TodoList } from '../redux/todoList/reducer';
import { Avatar, Collapse, Fab, List, ListItem, ListItemAvatar, ListItemText } from '@mui/material'
import DeleteIcon from '@mui/icons-material/Delete';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ExpandLessIcon from '@mui/icons-material/ExpandLess';
import { removeList } from '../redux/todoList/thunk';

interface Props {
    list: TodoList
}

function TodoListsDisplay(props: Props) {
    const dispatch = useDispatch();
    const todoTasks = useSelector((state: RootState) => state.todoList.tasks);
    const [open, setOpen] = useState(true);
    const handleClick = () => {
        setOpen(!open);
    };

    return (
        <List>
            <ListItem>
                <ListItemAvatar className="list-title" onClick={handleClick}>
                    <Avatar>
                        {open ? <ExpandLessIcon /> : <ExpandMoreIcon />}
                    </Avatar>
                </ListItemAvatar>
                <ListItemText primary={
                    <div className="list-title-container">
                        <div onClick={handleClick}>{props.list.name}</div>
                        <div className="list-del-icon">
                            <Fab size="small" color="primary" aria-label="delete" onClick={() => { dispatch(removeList(props.list.id)) }}>
                                <DeleteIcon />
                            </Fab>
                        </div>
                    </div>
                } />
            </ListItem>
            <Collapse in={open} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>

                    {todoTasks.filter((task => task.list_id === props.list.id)).map((task) => {
                        return (
                            <TodoTasksDisplay task={task} key={task.id} />
                        )
                    })}
                </List>
                <TodoTaskForm id={props.list.id} />
            </Collapse>
        </List>
    )
}

export default TodoListsDisplay;