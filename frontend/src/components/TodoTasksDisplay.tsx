import React, { useState } from 'react';
import "./TodoTasksDisplay.css"
import { Checkbox, Fab, FormControl, FormControlLabel, FormHelperText, InputLabel, ListItem, ListItemText, MenuItem, Select, SelectChangeEvent } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { Task as TodoTask } from "../redux/todoList/reducer";
import { completeTask, moveTask, removeTask } from '../redux/todoList/thunk';
import DoneIcon from '@mui/icons-material/Done';
import EditIcon from '@mui/icons-material/Edit';
import DriveFileMoveIcon from '@mui/icons-material/DriveFileMove';
import DeleteIcon from '@mui/icons-material/Delete';
import { useForm, Controller } from 'react-hook-form';
import { TodoTaskFormValues } from './TodoTaskForm';
import { selectedTask } from '../redux/todoList/actions';
import { RootState } from '../store';
import TodoTaskEditForm from './TodoTaskEditForm';
import EditOffIcon from '@mui/icons-material/EditOff';
import { format } from 'date-fns';

interface Props {
    task: TodoTask
}

export interface EditMultipleTaskForm {
    id: number;
    state: boolean;
}

function TodoTasksDisplay(props: Props) {
    const dispatch = useDispatch();
    const todoLists = useSelector((state: RootState) => state.todoList.lists);
    const [selectedList, setList] = useState('');
    const [showEdit, setShowEdit] = useState(false);
    const [showError, setShowError] = useState(false);

    const { control } = useForm<TodoTaskFormValues>();

    const handleChange = (event: SelectChangeEvent) => {
        setList(event.target.value as string);
    };

    return (
        <ListItem>
            <ListItemText
                primary={
                    <div className="task-title">
                        {showEdit ?
                            <TodoTaskEditForm id={props.task.id} onEdit={() => { setShowEdit(!showEdit) }} />
                            :
                            <div>
                                <FormControlLabel
                                    control={
                                        <Controller
                                            name="name"
                                            control={control}
                                            render={({ field }) =>
                                                <Checkbox
                                                    {...field}
                                                    checked={props.task.isSelected} onChange={() => {
                                                        dispatch(selectedTask(props.task.id))
                                                    }}
                                                />
                                            }
                                        />
                                    }
                                    label={
                                        <div className="task-label-elem">
                                            <div>
                                                <div>{props.task.name}</div>
                                                <div className="task-description">{props.task.description}</div>
                                            </div>
                                            {<div className={(new Date(props.task.deadline) > new Date()) ? "task-action-elem task-valid" : "task-action-elem task-invalid"}>deadline: {format(new Date(props.task.deadline), 'yyyy-MM-dd HH:mm')}</div>}
                                            {props.task.status === 'completed' && <div className="task-action-elem task-status">{props.task.status}</div>}
                                        </div>
                                    }
                                />
                            </div>}


                        <div className="task-action-container">
                            <div className="task-action-elem">
                                {props.task.status === 'to_do' && <Fab size="small"
                                    aria-label="done"
                                    onClick={() => { dispatch(completeTask(props.task.id)) }}
                                >
                                    <DoneIcon />
                                </Fab>}
                            </div>
                            <div className="task-action-elem">
                                <Fab
                                    size="small"
                                    color="primary"
                                    aria-label="edit"
                                    onClick={() => { setShowEdit(!showEdit) }}
                                >
                                    {showEdit ? <EditOffIcon /> : <EditIcon />}
                                </Fab>
                            </div>
                            <div className="task-action-elem">
                                <FormControl sx={{ minWidth: 100 }} required>
                                    <InputLabel id="demo-simple-select-label">Lists</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        value={selectedList}
                                        label="List"
                                        onChange={handleChange}

                                    >
                                        {todoLists.length !== 0 && todoLists.filter(item => item.id !== props.task.list_id).map((list) => {
                                            return (
                                                <MenuItem key={list.id} value={list.id}>{list.name}</MenuItem>
                                            )
                                        })}

                                    </Select>
                                    {showError && <FormHelperText>Required</FormHelperText>}
                                </FormControl>
                            </div>
                            <div className="task-action-elem">
                                <Fab
                                    size="small"
                                    color="primary"
                                    aria-label="delete"
                                    onClick={() => {
                                        if (selectedList) {
                                            dispatch(moveTask(props.task.id, parseInt(selectedList)));
                                            setShowError(false);
                                        } else {
                                            setShowError(true);
                                        }
                                    }}
                                >
                                    <DriveFileMoveIcon />
                                </Fab>
                            </div>
                            <div className="task-action-elem">
                                <Fab
                                    size="small"
                                    color="secondary"
                                    aria-label="delete"
                                    onClick={() => { dispatch(removeTask(props.task.id)) }}
                                >
                                    <DeleteIcon />
                                </Fab>
                            </div>
                        </div>
                    </div>
                }
            />
        </ListItem>
    )
}

export default TodoTasksDisplay;