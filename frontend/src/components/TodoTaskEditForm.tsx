import React from 'react';
import "./TodoTaskEditForm.css"
import { editTask } from '../redux/todoList/thunk';
import { useDispatch, useSelector } from 'react-redux';
import { Controller, SubmitHandler, useForm } from 'react-hook-form';
import { Fab, ListItem, ListItemIcon, ListItemText, TextField } from '@mui/material';
import { add } from "date-fns";
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DateTimePicker from '@mui/lab/DateTimePicker';
import DoneAllIcon from '@mui/icons-material/DoneAll';
import { RootState } from '../store';

interface Props {
    id: number
    onEdit: () => void
}

export interface TodoTaskEditFormValues {
    id: number;
    name: string;
    description?: string;
    deadline: Date;
}

function TodoTaskEditForm(props: Props) {
    const dispatch = useDispatch();
    const todoTasks = useSelector((state: RootState) => state.todoList.tasks.filter(task => task.id === props.id));

    const { control, setValue, handleSubmit, formState: { errors } } = useForm<TodoTaskEditFormValues>({
        defaultValues: {
            name: todoTasks[0].name,
            description: todoTasks[0].description || "",
            deadline: add(new Date(), { days: 1 })
        }
    });

    const onSubmit: SubmitHandler<TodoTaskEditFormValues> = data => {
        dispatch(editTask(data));
        setValue('name', '');
        setValue('description', '');
        setValue('deadline', add(new Date(), { days: 1 }));
        props.onEdit();
    };

    return (
        <ListItem component="div">
            <ListItemText
                primary={
                    <LocalizationProvider dateAdapter={AdapterDateFns}>
                        <div>
                            <form onSubmit={handleSubmit(onSubmit)}>

                                <div className="task-edit-form-elem">
                                    <Controller
                                        name="name"
                                        control={control}
                                        rules={{ required: "Name is required" }}
                                        render={({ field }) => <TextField
                                            {...field}
                                            error={errors.name && true}
                                            id="name"
                                            label="Name"
                                            variant="outlined"
                                            size="small"
                                            helperText={errors.name && `${errors.name.message}`}
                                        />}
                                    />
                                </div>

                                <div className="task-edit-form-elem">
                                    <Controller
                                        name="description"
                                        control={control}
                                        render={({ field }) => <TextField
                                            {...field}
                                            id="description"
                                            label="Description"
                                            variant="outlined"
                                            size="small"
                                        />}
                                    />
                                </div>

                                <div className="task-edit-form-elem">
                                    <Controller
                                        name="deadline"
                                        control={control}
                                        render={({ field: { onChange, value } }) => <DateTimePicker
                                            label="Deadline"
                                            value={value}
                                            onChange={onChange}
                                            renderInput={(params) => <TextField
                                                {...params}
                                                size="small"
                                            />}
                                        />}
                                    />
                                </div>

                                <ListItemIcon>
                                    <Fab
                                        size="small"
                                        color="primary"
                                        aria-label="done"
                                        onClick={() => { setValue("id", props.id) }}
                                        type="submit"
                                    >
                                        <DoneAllIcon />
                                    </Fab>
                                </ListItemIcon>
                            </form>
                        </div>
                    </LocalizationProvider>
                }
            />
        </ListItem>
    )
}

export default TodoTaskEditForm
