import { List, Task } from "./reducer";

export function gotTodoList(lists: List[]) {
    return {
        type: '@@todolist/GOT_TODO_LIST' as const,
        lists,
    }
};

export function getTodoListFailed(msg: string) {
    return {
        type: '@@todolist/GET_TODO_LIST_FAILED' as const,
        msg
    }
};

export function selectedList(id: number) {
    return {
        type: '@@todolist/SELECTED_LIST' as const,
        id,
    }
};

export function gotTodoTasks(tasks: Task[]) {
    return {
        type: '@@todolist/GOT_TODO_TASKS' as const,
        tasks
    }
};

export function getTodoTasksFailed(msg: string) {
    return {
        type: '@@todolist/GET_TODO_TASKS_FAILED' as const,
        msg
    }
};

export function addedTodoList() {
    return {
        type: '@@todolist/ADDED_TODO_LIST' as const,
    }
};

export function addTodoListFailed(msg: string) {
    return {
        type: '@@todolist/ADD_TODO_LIST_FAILED' as const,
        msg
    }
};

export function addedTodoTask() {
    return {
        type: '@@todolist/ADDED_TODO_TASK' as const,
    }
};

export function addTodoTaskFailed(msg: string) {
    return {
        type: '@@todolist/ADD_TODO_TASK_FAILED' as const,
        msg
    }
};

export function selectedTask(id: number) {
    return {
        type: '@@todolist/SELECTED_TASK' as const,
        id
    }
}

export function completedTask() {
    return {
        type: '@@todolist/COMPLETED_TASKS' as const,
    }
};

export function completeTaskFailed(msg: string) {
    return {
        type: '@@todolist/COMPLETE_TASK_FAILED' as const,
        msg
    }
};

export type TodoListActions = ReturnType<
    typeof gotTodoList |
    typeof getTodoListFailed |
    typeof selectedList |
    typeof gotTodoTasks |
    typeof getTodoTasksFailed |
    typeof selectedTask |
    typeof addedTodoList |
    typeof addTodoListFailed |
    typeof addedTodoTask |
    typeof addTodoTaskFailed |
    typeof completedTask |
    typeof completeTaskFailed
>;