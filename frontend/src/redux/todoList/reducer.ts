import produce from "immer";
import { TodoListActions } from "./actions";

export interface List {
    id: number;
    name: string;
    isOpened: boolean;
}

export interface Task {
    id: number;
    list_id: number;
    name: string;
    description: string;
    status: string;
    deadline: Date;
    isSelected: boolean;
    hasPassedDeadline: boolean;
}

export interface TodoListState {
    lists: List[]
    tasks: Task[]
    msg: string
}

const initialState: TodoListState = {
    lists: [],
    tasks: [],
    msg: ""
}

export function todoListReducer(state: TodoListState = initialState, action: TodoListActions): TodoListState {
    switch (action.type) {
        case '@@todolist/GOT_TODO_LIST':
            // let newLists: List[] = [];
            // for (let list of action.lists) {
            //     const originState = state.lists.filter(item => item.id === list.id)

            //     if (originState[0] !== undefined) {
            //         newLists.push({ id: list.id, name: list.name, isOpened: originState[0].isOpened })
            //     } else {
            //         newLists.push({ id: list.id, name: list.name, isOpened: false })
            //     }
            // }

            return produce(state, state => {
                state.lists = action.lists;
            })

        case '@@todolist/GOT_TODO_TASKS':
            let newTasks: Task[] = [];
            for (let task of action.tasks) {
                const originState = state.tasks.filter(item => item.id === task.id)

                if (originState[0] !== undefined) {
                    if ((new Date(task.deadline) < new Date())) {
                        newTasks.push({ ...task, isSelected: originState[0].isSelected, hasPassedDeadline: true })
                    } else {
                        newTasks.push({ ...task, isSelected: originState[0].isSelected, hasPassedDeadline: false })
                    }
                } else {
                    if ((new Date(task.deadline) < new Date())) {
                        newTasks.push({ ...task, isSelected: false, hasPassedDeadline: true })
                    } else {
                        newTasks.push({ ...task, isSelected: false, hasPassedDeadline: false })
                    }
                }
            }
            return produce(state, state => {
                state.tasks = newTasks;
            })

        // case '@@todolist/SELECTED_LISTS':
        //     const index = state.lists.findIndex(elem => elem.id === action.id)
        //     return produce(state, state => {
        //         state.lists[index].isOpened = !state.lists[index].isOpened;
        //     })

        case '@@todolist/SELECTED_TASK':
            const index = state.tasks.findIndex(elem => elem.id === action.id)

            return produce(state, state => {
                state.tasks[index].isSelected = !state.tasks[index].isSelected;
            })


        case '@@todolist/GET_TODO_LIST_FAILED':
            return produce(state, state => {
                state.msg = action.msg;
            })

        case '@@todolist/GET_TODO_TASKS_FAILED':
            return produce(state, state => {
                state.msg = action.msg;
            })

        case '@@todolist/COMPLETE_TASK_FAILED':
            return produce(state, state => {
                state.msg = action.msg;
            })

        default:
            return state;
    }
};