import { TodoListFormValues } from "../../components/TodoListForm";
import { TodoTaskEditFormValues } from "../../components/TodoTaskEditForm";
import { TodoTaskFormValues } from "../../components/TodoTaskForm";
import { env } from "../../env";
import { RootState, RootThunkDispatch } from "../../store";
import { addedTodoList, addedTodoTask, addTodoListFailed, addTodoTaskFailed, completedTask, completeTaskFailed, getTodoListFailed, getTodoTasksFailed, gotTodoList, gotTodoTasks } from "./actions";

export function fetchTodoList() {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            const userId = getState().auth.user?.user_id;

            if (userId) {
                const res = await fetch(`${env.REACT_APP_API_SERVER}/lists/user/${userId}`, {
                    method: "GET",
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                        "Authorization": `Bearer ${localStorage.getItem('token')}`
                    },
                });

                const result = await res.json();

                if (res.status !== 200 || !result.success) {
                    dispatch(getTodoListFailed(result.msg));
                } else {
                    dispatch(gotTodoList(result.data));
                }
            } else {
                dispatch(getTodoListFailed("User ID is undefined"));
            }
        } catch (error) {
            return;
        }
    };
}

export function fetchTodoTask() {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            const userId = getState().auth.user?.user_id;

            if (userId) {
                const res = await fetch(`${env.REACT_APP_API_SERVER}/tasks/user/${userId}`, {
                    method: "GET",
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                        "Authorization": `Bearer ${localStorage.getItem('token')}`
                    },
                });

                const result = await res.json();

                if (res.status !== 200 || !result.success) {
                    dispatch(getTodoTasksFailed(result.msg));
                } else {
                    dispatch(gotTodoTasks(result.data));
                }
            } else {
                dispatch(getTodoTasksFailed("User ID is undefined"));
            }
        } catch (error) {
            return;
        }
    };
}

export function addTodoList(formData: TodoListFormValues) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            const userId = getState().auth.user?.user_id;

            const todoListForm = {
                id: userId,
                name: formData.name
            };

            const res = await fetch(`${env.REACT_APP_API_SERVER}/list`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    "Authorization": `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify(todoListForm)
            });

            const result = await res.json();

            if (res.status !== 200 || !result.success) {
                dispatch(addTodoListFailed(result.msg));
            } else {
                dispatch(addedTodoList());
            }
        } catch (error) {
            return;
        }
    };
}

export function addTodoTask(formData: TodoTaskFormValues) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            const userId = getState().auth.user?.user_id;

            const newFormData = {
                ...formData,
                userId
            };

            const res = await fetch(`${env.REACT_APP_API_SERVER}/task`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    "Authorization": `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify(newFormData)
            });

            const result = await res.json();

            if (res.status !== 200 || !result.success) {
                dispatch(addTodoTaskFailed(result.msg));
            } else {
                dispatch(addedTodoTask());
            }
        } catch (error) {
            return;
        }
    };
}

export function editTask(formData: TodoTaskEditFormValues) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            const userId = getState().auth.user?.user_id;

            const newFormData = {
                ...formData,
                userId
            };

            const res = await fetch(`${env.REACT_APP_API_SERVER}/task/edit`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    "Authorization": `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify(newFormData)
            });

            await res.json();
        } catch (error) {
            return;
        }
    };
}

export function moveTask(taskId: number, listId: number) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            const userId = getState().auth.user?.user_id;

            const formData = {
                taskId,
                listId,
                userId
            }
            const res = await fetch(`${env.REACT_APP_API_SERVER}/task/move`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    "Authorization": `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify(formData)
            });

            await res.json();
        } catch (error) {
            return;
        }
    };
}

export function moveTasks(listId: number) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            const ids = getState().todoList.tasks.filter(task => task.isSelected === true).map(task => task.id)
            const userId = getState().auth.user?.user_id;

            const formData = {
                taskId: ids,
                listId: listId,
                userId
            }

            const res = await fetch(`${env.REACT_APP_API_SERVER}/tasks/move`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    "Authorization": `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify(formData)
            });

            await res.json();
        } catch (error) {

            return;
        }
    };
}

export function completeTask(id: number) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            const userId = getState().auth.user?.user_id;

            const formData = {
                userId
            }

            const res = await fetch(`${env.REACT_APP_API_SERVER}/task/complete/${id}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    "Authorization": `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify(formData)
            });

            const result = await res.json();

            if (result.success) {
                dispatch(completedTask());
            } else {
                dispatch(completeTaskFailed(result.msg));
            }
        } catch (error) {
            return;
        }
    };
}

export function removeList(id: number) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            const userId = getState().auth.user?.user_id;

            const formData = {
                userId
            }

            const res = await fetch(`${env.REACT_APP_API_SERVER}/list/${id}`, {
                method: "DELETE",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    "Authorization": `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify(formData)
            });

            await res.json();
        } catch (error) {
            return;
        }
    }
}

export function removeTask(id: number) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            const userId = getState().auth.user?.user_id;

            const formData = {
                userId
            }

            const res = await fetch(`${env.REACT_APP_API_SERVER}/task/${id}`, {
                method: "DELETE",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    "Authorization": `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify(formData)
            });

            await res.json();
        } catch (error) {
            return;
        }
    }
}

export function removeTasks() {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            const userId = getState().auth.user?.user_id;
            const ids = getState().todoList.tasks.filter(task => task.isSelected === true).map(task => task.id)
            const formData = { ids: ids, userId };
            const res = await fetch(`${env.REACT_APP_API_SERVER}/tasks`, {
                method: "DELETE",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    "Authorization": `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify(formData)
            });

            await res.json();
        } catch (error) {
            return;
        }

    }
}

export function checkDeadline() {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            const currentTasks = getState().todoList.tasks;
            let refresh = false;
            for (let currentTask of currentTasks) {
                if ((new Date(currentTask.deadline) < new Date()) && !currentTask.hasPassedDeadline) {
                    console.log(`${currentTask.name} has passed deadline!`);
                    refresh = true;
                }
            }

            if (refresh) {
                dispatch(fetchTodoTask());
                refresh = false;
            }

        } catch (error) {
            return;
        }
    }
}
