export function loginSuccess() {
    return {
        type: '@@auth/LOGIN_SUCCESS' as const,
    }
};

export function loginFailed(msg: string) {
    return {
        type: '@@auth/LOGIN_FAILED' as const,
        msg
    }
};

export function logoutSuccess() {
    return {
        type: '@@auth/LOGOUT' as const,
    }
};

export function loadToken() {
    return {
        type: '@@auth/LOAD_TOKEN' as const,
    }
}

export function registerSuccess() {
    return {
        type: '@@auth/REGISTERSUCCESS' as const,
    }
};

export function registerFailed(msg: string) {
    return {
        type: '@@auth/REGISTER_FAILED' as const,
        msg
    }
};

export type AuthActions = ReturnType<typeof loginSuccess | typeof loginFailed | typeof logoutSuccess | typeof loadToken | typeof registerSuccess | typeof registerFailed>;