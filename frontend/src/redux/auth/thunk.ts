import { push } from "redux-first-history";
import { env } from "../../env";
import { LoginFormValues } from "../../pages/LoginPage";
import { RegisterFormValues } from "../../pages/RegisterPage";
import { RootState, RootThunkDispatch } from "../../store";
import { loginFailed, loginSuccess, logoutSuccess, registerFailed, registerSuccess } from "./actions";

export function login(formData: LoginFormValues) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            const res = await fetch(`${env.REACT_APP_API_SERVER}/login`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify(formData),
            });

            const result = await res.json();

            if (res.status !== 200) {
                dispatch(loginFailed(result.msg));
            } else {
                localStorage.setItem('token', result.token)
                dispatch(loginSuccess());
            }
        } catch (error) {
            return;
        }
    };
}

export function logout() {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        dispatch(logoutSuccess());
        localStorage.removeItem('token');
        dispatch(push('/'));
    };
}

export function register(formData: RegisterFormValues) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            const res = await fetch(`${env.REACT_APP_API_SERVER}/register`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify(formData),
            });

            const result = await res.json();

            if (res.status !== 200) {
                dispatch(registerFailed(result.msg));
            } else {
                dispatch(registerSuccess());
                dispatch(push('/'));
            }
        } catch (error) {
            return;
        }
    };
}