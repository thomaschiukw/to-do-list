import produce from "immer";
import jwt_decode from "jwt-decode";
import { AuthActions } from "./actions";

export interface JWTPayload {
    user_id: number;
    username: string;
};

export interface AuthState {
    isAuthenticated: boolean;
    authMsg: string;
    user?: JWTPayload;
    tokenMsg?: string;
    regMsg: string;
};

const initialState: AuthState = {
    isAuthenticated: (localStorage.getItem('token') !== null),
    authMsg: "",
    regMsg: ""
};

export function authReducer(state: AuthState = initialState, action: AuthActions): AuthState {
    switch (action.type) {
        case '@@auth/LOGIN_SUCCESS':
            return produce(state, state => {
                state.isAuthenticated = true;
                state.authMsg = "";
            })

        case '@@auth/LOGIN_FAILED':
            return produce(state, state => {
                state.isAuthenticated = false;
                state.authMsg = action.msg;
            })

        case '@@auth/LOGOUT':
            return produce(state, state => {
                state.isAuthenticated = false;
                state.authMsg = "";
            })

        case '@@auth/LOAD_TOKEN':
            const token = localStorage.getItem('token');
            if (token) {
                let payload: JWTPayload = jwt_decode(token);

                if (payload) {
                    return produce(state, state => {
                        state.isAuthenticated = true;
                        state.user = payload;
                        state.tokenMsg = undefined;
                    })
                } else {
                    return produce(state, state => {
                        state.isAuthenticated = false;
                        state.user = undefined;
                        state.tokenMsg = 'Invalid JWT Token';
                    })
                }
            } else {
                return produce(state, state => {
                    state.user = undefined;
                    state.tokenMsg = 'Invalid JWT Token';
                })
            }

        case '@@auth/REGISTER_FAILED':
            return produce(state, state => {
                state.regMsg = action.msg;
            })

        default:
            return state;
    }
};