import dotenv from "dotenv";
dotenv.config();

export const env = {
    REACT_APP_API_SERVER: process.env.REACT_APP_API_SERVER,
}