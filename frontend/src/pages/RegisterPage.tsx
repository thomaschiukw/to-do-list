import "./RegisterPage.css"
import { TextField, Button, Alert, Snackbar } from "@mui/material";
import { useEffect, useState } from "react";
import { useForm, SubmitHandler, Controller } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { register } from "../redux/auth/thunk";
import { RootState } from "../store";

export interface RegisterFormValues {
    username: string;
    email: string;
    password: string;
}

export default function RegisterPage() {
    const dispatch = useDispatch();
    const errorMessage = useSelector((state: RootState) => state.auth.regMsg);

    const { control, handleSubmit, formState: { errors } } = useForm<RegisterFormValues>({
        defaultValues: {
            username: '',
            email: '',
            password: ''
        }
    });

    const onSubmit: SubmitHandler<RegisterFormValues> = data => {
        dispatch(register(data));
    };

    const [open, setOpen] = useState(false);

    const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    useEffect(() => {
        if (errorMessage) {
            setOpen(true)
        }

        return () => {
            setOpen(false)
        }
    }, [errorMessage])

    return (
        <form onSubmit={handleSubmit(onSubmit)} className="registerFormContainer">
            <div className="registerFormElem">
                <Controller
                    name="username"
                    control={control}
                    rules={{ required: "Username is required" }}
                    render={({ field }) => <TextField
                        {...field}
                        fullWidth
                        error={errors.username && true}
                        id="username"
                        label="Username"
                        variant="outlined"
                        helperText={errors.username && `${errors.username.message}`}
                    />}
                />
            </div>

            <div className="registerFormElem">
                <Controller
                    name="email"
                    control={control}
                    rules={{ required: "Email is required" }}
                    render={({ field }) => <TextField
                        {...field}
                        fullWidth
                        error={errors.email && true}
                        id="email"
                        label="Email"
                        type="email"
                        variant="outlined"
                        helperText={errors.email && `${errors.email.message}`}
                    />}
                />
            </div>

            <div className="registerFormElem">
                <Controller
                    name="password"
                    control={control}
                    rules={{ required: "Password is required" }}
                    render={({ field }) => <TextField
                        {...field}
                        fullWidth
                        id="password"
                        label="Password"
                        variant="outlined"
                        type="password"
                        error={errors.password && true}
                        helperText={errors.password && `${errors.password.message}`}
                    />}
                />
            </div>

            <div className="registerFormElem">
                <Button
                    type="submit"
                    variant="contained"
                    className="registerButtonElem"
                >
                    Register
                </Button>
                <Button
                    type="reset"
                    variant="contained"
                    className="registerButtonElem"
                >
                    Reset
                </Button>
            </div>

            <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="error" sx={{ width: '100%' }}>
                    {errorMessage}
                </Alert>
            </Snackbar>
        </form>
    );
}
