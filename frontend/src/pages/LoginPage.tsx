import "./LoginPage.css"
import { Controller, SubmitHandler, useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { login } from "../redux/auth/thunk";
import { useEffect, useState } from "react";
import { push } from "redux-first-history";
import { RootState } from "../store";
import { useLocation } from "react-router-dom";
import { loadToken } from "../redux/auth/actions";
import { Alert, Button, Snackbar, TextField } from "@mui/material";

export interface LoginFormValues {
    username: string;
    password: string;
}

export default function LoginPage() {
    const dispatch = useDispatch();
    const { state } = useLocation();
    const isAuth = useSelector((state: RootState) => state.auth.isAuthenticated);
    const errorMessage = useSelector((state: RootState) => state.auth.authMsg);

    const { control, handleSubmit, formState: { errors } } = useForm<LoginFormValues>({
        defaultValues: {
            username: '',
            password: ''
        }
    });

    const onSubmit: SubmitHandler<LoginFormValues> = data => {
        dispatch(login(data));
    };

    useEffect(() => {
        if (isAuth) {
            dispatch(loadToken());
            if (state === null) {
                dispatch(push("/tasks"));
            } else {
                dispatch(push(state.path));
            }
        }
    }, [dispatch, isAuth, state])

    const [open, setOpen] = useState(false);

    const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    useEffect(() => {
        if (errorMessage) {
            setOpen(true)
        }

        return () => {
            setOpen(false)
        }
    }, [errorMessage])

    return (
        <form onSubmit={handleSubmit(onSubmit)} className="loginFormContainer">
            <div className="loginFormElem">
                <Controller
                    name="username"
                    control={control}
                    rules={{ required: "Username is required" }}
                    render={({ field }) => <TextField
                        {...field}
                        fullWidth
                        error={errors.username && true}
                        id="username"
                        label="Username"
                        variant="outlined"
                        helperText={errors.username && `${errors.username.message}`}
                    />}
                />
            </div>

            <div className="loginFormElem">
                <Controller
                    name="password"
                    control={control}
                    rules={{ required: "Password is required" }}
                    render={({ field }) => <TextField
                        {...field}
                        fullWidth
                        id="password"
                        label="Password"
                        variant="outlined"
                        type="password"
                        error={errors.password && true}
                        helperText={errors.password && `${errors.password.message}`}
                    />}
                />
            </div>

            <div className="loginFormElem">
                <Button
                    type="submit"
                    variant="contained"
                >
                    Login
                </Button>
            </div>
            <div>
                <Button
                    variant="contained"
                    onClick={() => { dispatch(push("/register")) }}
                >
                    Register
                </Button>
            </div>

            <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="error" sx={{ width: '100%' }}>
                    {errorMessage}
                </Alert>
            </Snackbar>
        </form>
    );
}