import { logout } from "../redux/auth/thunk";
import { useDispatch } from "react-redux";
import { Button } from "@mui/material";

export default function SettingsPage() {
    const dispatch = useDispatch();
    return (
        <div>
            <Button variant="contained" onClick={() => dispatch(logout())}>Logout</Button>
        </div>
    )
}