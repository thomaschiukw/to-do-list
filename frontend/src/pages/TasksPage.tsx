import React from "react";
import "./TasksPage.css"
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../store";
import { fetchTodoList, fetchTodoTask, moveTasks, removeTasks } from "../redux/todoList/thunk";
import TodoListsDisplay from "../components/TodoListsDisplay";
import { Container, Fab, FormControl, FormHelperText, InputLabel, MenuItem, Select, SelectChangeEvent } from "@mui/material";
import TodoListForm from "../components/TodoListForm";
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import DriveFileMoveIcon from '@mui/icons-material/DriveFileMove';
import Snacker from "../components/Snacker";

export default function TasksPage() {
    const dispatch = useDispatch();
    const todoLists = useSelector((state: RootState) => state.todoList.lists);
    const selectedTodoTasks = useSelector((state: RootState) => state.todoList.tasks.filter(task => task.isSelected === true));
    const [openEditMenu, setOpenEditMenu] = useState(false);
    const [list, setList] = useState('');
    const [showError, setShowError] = useState(false);

    const handleChange = (event: SelectChangeEvent) => {
        setList(event.target.value as string);
    };

    useEffect(() => {
        dispatch(fetchTodoList());
        dispatch(fetchTodoTask());
    }, [dispatch])

    return (
        <Container>
            <Snacker />
            <TodoListForm />
            {selectedTodoTasks.length !== 0 &&
                <div className="move-tasks-container">

                    <div className="move-tasks-elem">
                        <Fab className="move-tasks-icon" size="small" color="secondary" aria-label="edit" onClick={() => { setOpenEditMenu(!openEditMenu) }}>
                            <EditIcon />
                        </Fab>
                        <Fab className="move-tasks-icon" size="small" color="primary" aria-label="delete" onClick={() => { dispatch(removeTasks()) }}>
                            <DeleteIcon />
                        </Fab>
                    </div>

                    {selectedTodoTasks.length !== 0 && openEditMenu &&
                        <div className="move-tasks-elem">
                            <FormControl sx={{ m: 1, minWidth: 200 }} className="move-tasks-icon" required >
                                <InputLabel id="list-select-label">Lists</InputLabel>
                                <Select
                                    labelId="list-select-label"
                                    id="list-select"
                                    value={list}
                                    label="List"
                                    onChange={handleChange}
                                >
                                    {todoLists.length !== 0 && todoLists.map((list) => {
                                        return <MenuItem key={list.id} value={list.id}>{list.name}</MenuItem>
                                    })}
                                </Select>
                                {showError && <FormHelperText>Required</FormHelperText>}
                            </FormControl>

                            <Fab size="small" color="primary" aria-label="delete"
                                onClick={() => {
                                    if (list) {
                                        dispatch(moveTasks(parseInt(list)))
                                        setShowError(false);
                                    } else {
                                        setShowError(true);
                                    }
                                }}>
                                <DriveFileMoveIcon />
                            </Fab>

                        </div>
                    }
                </div>
            }

            {todoLists.length !== 0 && todoLists.map((list) => {
                return <TodoListsDisplay list={list} key={list.id} />
            })}
        </Container>

    )
}