import React from 'react'
import { Link } from 'react-router-dom'

function NoMatch() {
    return (
        <div>
            <h1>404 - Not Found!</h1>
            <p><Link to="/tasks">Go to tasks</Link></p>
            <p><Link to="/settings">Go to settings</Link></p>
        </div>
    )
}

export default NoMatch
