import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
import { Provider } from 'react-redux';
import { store } from './store';
import { MemoryRouter } from 'react-router';

test('full app rendering/navigating', () => {
    render(
        <Provider store={store}>
            <MemoryRouter>
                <App />
            </MemoryRouter>
        </Provider>
    )
    expect(screen.getByText(/login/i)).toBeInTheDocument();
})