import React, { useEffect, useState } from 'react';
import './App.css';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from './store';
import { Route, Routes, useLocation } from 'react-router-dom';
import LoginPage from './pages/LoginPage';
import TasksPage from './pages/TasksPage';
import SettingsPage from './pages/SettingsPage';
import RegisterPage from './pages/RegisterPage';
import NoMatch from './pages/NoMatch';
import PrivateRoute from './PrivateRoute';
import { env } from './env';
import { io } from "socket.io-client";
import { push } from 'redux-first-history';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import TaskIcon from '@mui/icons-material/Task';
import SettingsIcon from '@mui/icons-material/Settings';
import { Container } from '@mui/material';
import { checkDeadline } from './redux/todoList/thunk';

export const socket = io(`${env.REACT_APP_API_SERVER}`);

function App() {
    const dispatch = useDispatch();
    const location = useLocation();
    const isAuth = useSelector((state: RootState) => state.auth.isAuthenticated);
    const userId = useSelector((state: RootState) => state.auth.user?.user_id);
    const [value, setValue] = useState(0);

    useEffect(() => {
        return () => {
            socket.disconnect();
        }
    }, [])

    useEffect(() => {
        let CronJob = require('cron').CronJob;
        let job = new CronJob(
            '* * * * * *',
            function () {
                dispatch(checkDeadline());
            },
            null,
            true,
        );
        job.start();
    }, [dispatch])

    useEffect(() => {
        if (userId) {
            socket.emit('join', { id: userId });
        }
    }, [userId])

    useEffect(() => {
        let path = location.pathname;
        if (path === "/tasks" && value !== 0) {
            setValue(0);
        } else if (path === "/settings" && value !== 1) {
            setValue(1)
        };

    }, [location.pathname, value]);

    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        setValue(newValue);
    };

    return (
        <Container maxWidth="xl">
            {isAuth && (location.pathname === "/tasks" || location.pathname === "/settings") &&
                <Tabs value={value} onChange={handleChange} aria-label="icon label tabs" className="MuiTabs-flexContainer">
                    <Tab icon={<TaskIcon />} label="Tasks" onClick={() => { dispatch(push('/tasks')) }} />
                    <Tab icon={<SettingsIcon />} label="Settings" onClick={() => { dispatch(push('/settings')) }} />
                </Tabs>
            }

            <Routes>
                <Route path="/" element={<LoginPage />} />
                <Route path="/register" element={<RegisterPage />} />
                <Route
                    path="/tasks"
                    element={
                        <PrivateRoute path="/tasks">
                            <TasksPage />
                        </PrivateRoute>
                    }
                />
                <Route
                    path="/settings"
                    element={
                        <PrivateRoute path="/settings">
                            <SettingsPage />
                        </PrivateRoute>
                    }
                />
                <Route path="*" element={<NoMatch />} />
            </Routes>
        </ Container>
    );
}

export default App;
