import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux';
import { store, history } from './store';
import { HistoryRouter as Router } from "redux-first-history/rr6";
import LogRocket from 'logrocket';
import setupLogRocketReact from 'logrocket-react';

LogRocket.init('aywpv8/to-do-list');
setupLogRocketReact(LogRocket);
LogRocket.identify('thomaschiu', {
    name: 'Thomas Chiu',
    email: 'thomaschiukw@gmail.com',

    // Add your own custom user variables here, ie:
    subscriptionType: 'pro'
});

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <Router history={history}>
                <App />
            </Router>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
