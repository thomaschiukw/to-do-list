import { useSelector } from "react-redux";
import { Navigate } from "react-router-dom";
import { RootState } from "./store";

interface Props {
    children: JSX.Element
    path?: string
}

export const PrivateRoute: React.FC<Props> = ({ children, path }) => {
    const isAuth = useSelector((state: RootState) => state.auth.isAuthenticated);
    const user = useSelector((state: RootState) => state.auth.user);

    if (isAuth && user) {
        return children
    } else {
        return <Navigate to="/" replace state={{ path }} />
    }
}

export default PrivateRoute;