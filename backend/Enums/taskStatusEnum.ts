export enum TaskStatusEnum {
    TO_DO = 'to_do',
    COMPLETED = 'completed'
}
