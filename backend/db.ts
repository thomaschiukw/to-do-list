import dotenv from 'dotenv';
import Knex from 'knex';
import { env } from './env';

dotenv.config();

const knexConfigs = require('./knexfile');
const configMode = env.NODE_ENV || "development";
const knexConfig = knexConfigs[configMode];
export const knex = Knex(knexConfig);