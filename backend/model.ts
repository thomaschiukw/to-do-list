export interface User {
    user_id: number
    username: string
}

declare global {
    namespace Express {
        interface Request {
            user?: User
        }
    }
}