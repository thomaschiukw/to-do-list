import { add } from 'date-fns';
import Knex from 'knex';
import { TaskStatusEnum } from '../Enums/taskStatusEnum';
import { TodoListService } from '../services/TodoListService';
const knexfile = require('../knexfile'); // Assuming you test case is inside `services/ folder`
const knex = Knex(knexfile["test"]); // Now the connection is a testing connection.


describe("TodoListService", () => {

    let todoListService: TodoListService;
    let id: number;
    beforeAll(async () => {
        await knex.migrate.rollback({}, true);
        await knex.migrate.latest();
        await knex.seed.run();
    });

    afterAll(async () => {
        await knex.migrate.rollback({}, true);
        await knex.migrate.latest();
        await knex.seed.run();
        knex.destroy();
    });

    beforeEach(async () => {
        todoListService = new TodoListService(knex);
        await knex('tasks').del();
        id = await knex.insert({
            list_id: 1,
            name: 'Send email',
            description: 'Send email to Thomas',
            status: TaskStatusEnum.TO_DO,
            deadline: add(new Date(), { minutes: 100 })
        }).into('tasks').returning('id');
    })

    it("should get all tasks", async () => {
        const tasks = await todoListService.getTasksByUser(1);
        expect(tasks.data?.length).toBe(1);
    });

    it("should update task", async () => {
        await todoListService.editTask(
            id[0],
            'test',
            new Date("2021-11-19T20:06:08.413Z"),
            'testing'
        );
        const tasks = await todoListService.getTasksByUser(1);
        expect(tasks.data![0]).toEqual({
            "id": id[0],
            "list_id": 1,
            "name": "test",
            "description": "testing",
            "status": TaskStatusEnum.TO_DO,
            "deadline": new Date("2021-11-19T20:06:08.413Z")
        });
    });

    it("should complete task", async () => {
        await todoListService.completeTask(id[0]);
        const tasks = await todoListService.getTasksByUser(1);
        expect(tasks.data![0].status).toBe(TaskStatusEnum.COMPLETED);
    });

    it("should remove all tasks", async () => {
        await todoListService.removeTask(id[0]);
        const tasks = await todoListService.getTasksByUser(1);
        expect(tasks.data?.length).toBe(0);
    });
})