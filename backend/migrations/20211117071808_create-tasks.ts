import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('tasks');
    if (!hasTable) {
        await knex.schema.createTable('tasks', (table) => {
            table.increments();
            table.integer("list_id").unsigned().notNullable();
            table.foreign('list_id').references('lists.id');
            table.string("name").notNullable();
            table.string("description");
            table.string('status').notNullable();
            table.datetime('deadline').notNullable();
            table.timestamps(false, true);
        });
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('tasks');
}

