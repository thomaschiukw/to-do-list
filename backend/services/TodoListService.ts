import { Knex } from 'knex';
import { TaskStatusEnum } from '../Enums/taskStatusEnum';
import { logger } from '../logger';

export interface MoveTask {
    taskId: number[]
    listId: number
}

export class TodoListService {
    constructor(private knex: Knex) { };

    getAllLists = async () => {
        try {
            const result = await this.knex.select("*").from('lists');
            return { success: true, data: result };
        } catch (error) {
            logger.error("Error: " + JSON.stringify(error.message));
            return { success: false, msg: error.message };
        }
    }

    getListsByUser = async (id: number) => {
        try {
            const result = await this.knex
                .select("lists.id", "lists.name")
                .from('lists')
                .where('user_id', id);
            return { success: true, data: result };
        } catch (error) {
            logger.error("Error: " + JSON.stringify(error.message));
            return { success: false, msg: error.message };
        }
    }

    getTasksByUser = async (id: number) => {
        try {
            const result = await this.knex
                .select("t.id", "t.list_id", "t.name", "t.description", "t.status", "t.deadline")
                .from('tasks as t')
                .innerJoin('lists as l', 't.list_id', 'l.id')
                .where('l.user_id', id)
                .orderBy('t.id');
            return { success: true, data: result };
        } catch (error) {
            logger.error("Error: " + JSON.stringify(error.message));
            return { success: false, msg: error.message };
        }
    }

    createList = async (id: number, name: string) => {
        try {
            await this.knex('lists').insert({ user_id: id, name: name });
            return { success: true };
        } catch (error) {
            logger.error("Error: " + JSON.stringify(error.message));
            return { success: false, msg: error.message };
        }
    }

    createTask = async (id: number, name: string, deadline: Date, description?: string) => {
        try {
            if (description) {
                await this.knex('tasks').insert({
                    list_id: id,
                    name: name,
                    description: description,
                    status: TaskStatusEnum.TO_DO,
                    deadline: deadline
                });
            } else {
                await this.knex('tasks').insert({
                    list_id: id,
                    name: name,
                    status: TaskStatusEnum.TO_DO,
                    deadline: deadline
                });
            }
            return { success: true };
        } catch (error) {
            logger.error("Error: " + JSON.stringify(error.message));
            return { success: false, msg: error.message };
        }
    }

    editTask = async (id: number, name: string, deadline: Date, description?: string) => {
        try {
            if (description) {
                await this.knex('tasks')
                    .where('id', id)
                    .update({
                        name,
                        description,
                        deadline,
                        updated_at: new Date()
                    });
            } else {
                await this.knex('tasks')
                    .where('id', id)
                    .update({
                        name,
                        deadline,
                        updated_at: new Date()
                    });
            }
            return { success: true };
        } catch (error) {
            logger.error("Error: " + JSON.stringify(error.message));
            return { success: false, msg: error.message };
        }
    }

    moveTask = async (taskId: number, listId: number) => {
        try {
            await this.knex('tasks')
                .where('id', taskId)
                .update({ list_id: listId, updated_at: new Date() });
            return { success: true };
        } catch (error) {
            logger.error("Error: " + JSON.stringify(error.message));
            return { success: false, msg: error.message };
        }
    }

    moveTasks = async (tasks: MoveTask) => {
        return this.knex.transaction(async (trx) => {
            try {
                for (const taskId of tasks.taskId) {
                    await trx('tasks')
                        .where('id', taskId)
                        .update({ list_id: tasks.listId, updated_at: new Date() });
                }
                return { success: true };
            } catch (error) {
                logger.error("Error: " + JSON.stringify(error.message));
                return { success: false, msg: error.message };
            }
        });
    }

    completeTask = async (id: number) => {
        try {
            await this.knex('tasks')
                .where('id', id)
                .update({ status: TaskStatusEnum.COMPLETED, updated_at: new Date() });
            return { success: true };
        } catch (error) {
            logger.error("Error: " + JSON.stringify(error.message));
            return { success: false, msg: error.message };
        }
    }

    removeList = async (id: number) => {
        const txn = await this.knex.transaction();
        try {
            await txn('tasks').where('list_id', id).del();
            await txn('lists').where('id', id).del();
            await txn.commit();
            return { success: true };
        } catch (error) {
            await txn.rollback();
            logger.error("Error: " + JSON.stringify(error.message));
            return { success: false, msg: error.message };
        }
    }

    removeTask = async (id: number) => {
        try {
            await this.knex('tasks').where('id', id).del();
            return { success: true };
        } catch (error) {
            logger.error("Error: " + JSON.stringify(error.message));
            return { success: false, msg: error.message };
        }
    }

    removeTasks = async (ids: number[]) => {
        return this.knex.transaction(async (trx) => {
            try {
                for (let id of ids) {
                    await trx('tasks')
                        .where('id', id)
                        .del();
                }
                return { success: true };
            } catch (error) {
                logger.error("Error: " + JSON.stringify(error.message));
                return { success: false, msg: error.message };
            }
        });
    }
}