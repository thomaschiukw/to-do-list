import { Knex } from 'knex';
import { hashPassword } from '../hash';

export class UserService {
    constructor(private knex: Knex) { };

    getUser = async (username: string) => {
        const user = await this.knex.select("*").from('users').where('username', username);
        return user;
    }

    getUserById = async (id: number) => {
        try {
            const result = await this.knex.select("*").from('users').where('id', id);
            return result;
        } catch (error) {
            return { success: false, msg: error.message };
        }
    }

    register = async (username: string, email: string, password: string) => {
        try {
            const hashedPassword = await hashPassword(password);

            const sameNameUsers = await this.knex.select("*").from('users').where('username', username);
            if (sameNameUsers.length > 0) {
                return { success: false, msg: "The username has been used already!" };
            }

            await this.knex("users").insert([{
                username: username,
                email: email,
                password: hashedPassword,
            }]);

            return { success: true };
        } catch (error) {
            return { success: false, msg: error.message };
        }
    }
}