import { Request, Response } from 'express';
import { UserService } from '../services/UserService';
import { checkPassword } from '../hash';
import jwtSimple from 'jwt-simple';
import jwt from '../jwt';

export class AuthController {
    constructor(private userService: UserService) { }

    login = async (req: Request, res: Response) => {
        try {
            if (!req.body.username || !req.body.password) {
                return res.status(400).json({ success: false, msg: 'No username or password' });
            }

            const { username, password } = req.body;
            const user = await this.userService.getUser(username);

            if (user[0] == null || !await checkPassword(password, user[0].password)) {
                return res.status(401).json({ success: false, msg: 'Invalid username or password' });
            }

            const payload = {
                user_id: user[0].id,
                username: user[0].username,
            };

            const token = jwtSimple.encode(payload, jwt.jwtSecret);

            if (token) {
                return res.json({ token: token });
            } else {
                return res.status(401).json({ success: false, msg: 'Undefined token' });
            }
        } catch (error) {
            return res.status(500).json({ success: false, error });
        }
    }

    register = async (req: Request, res: Response) => {
        try {
            const { username, email, password } = req.body;

            if (!username || !email || !password) {
                return res.status(400).json({ success: false, msg: 'Undefined' });
            }

            const result = await this.userService.register(username, email, password);

            if (result.success) {
                return res.json(result);
            } else {
                return res.status(500).json(result);
            }
        } catch (error) {
            return res.status(500).json({ success: false, msg: error.message });
        }
    }
}