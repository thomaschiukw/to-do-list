import { Request, Response } from 'express';
import { TodoListService } from '../services/TodoListService';
import SocketIO from "socket.io";
import { logger } from '../logger';

export class TodoListController {
    constructor(private todoListService: TodoListService, private io: SocketIO.Server) { }

    getAllLists = async (req: Request, res: Response) => {
        try {
            const result = await this.todoListService.getAllLists();

            if (result.success) {
                return res.json(result);
            } else {
                return res.status(500).json(result);
            }
        } catch (error) {
            return res.status(500).json({ success: false, msg: error.message });
        }
    }

    getListsByUser = async (req: Request, res: Response) => {
        try {
            const id = parseInt(req.params.id);

            if (!id) {
                return res.status(400).json({ success: false, msg: 'Undefined' });
            }

            if (isNaN(id) || typeof id !== 'number') {
                return res.status(400).json({ success: false, msg: 'Type Error' });
            }

            const result = await this.todoListService.getListsByUser(id);

            if (result.success) {
                return res.json(result);
            } else {
                return res.status(500).json(result);
            }
        } catch (error) {
            return res.status(500).json({ success: false, msg: error.message });
        }
    }

    getTasksByUser = async (req: Request, res: Response) => {
        try {
            const id = parseInt(req.params.id);

            if (!id) {
                return res.status(400).json({ success: false, msg: 'Undefined' });
            }

            if (isNaN(id) || typeof id !== 'number') {
                return res.status(400).json({ success: false, msg: 'Type Error' });
            }

            const result = await this.todoListService.getTasksByUser(id);

            if (result.success) {
                return res.json(result);
            } else {
                return res.status(500).json(result);
            }
        } catch (error) {
            return res.status(500).json({ success: false, msg: error.message });
        }
    }

    createList = async (req: Request, res: Response) => {
        try {
            const id: number = parseInt(req.body.id);
            const name: string = req.body.name;

            if (!id || !name) {
                return res.status(400).json({ success: false, msg: 'Undefined' });
            }

            if (isNaN(id) || typeof id !== 'number' || typeof name !== 'string') {
                return res.status(400).json({ success: false, msg: 'Type Error' });
            }

            const result = await this.todoListService.createList(id, name);

            if (result.success) {
                this.io.to((id.toString())).emit("list-created");
                return res.json(result);
            } else {
                return res.status(500).json(result);
            }
        } catch (error) {
            return res.status(500).json({ success: false, msg: error.message });
        }
    }

    createTask = async (req: Request, res: Response) => {
        try {
            const id: number = parseInt(req.body.id);
            const name: string = req.body.name;
            const description: string = req.body.description;
            const deadline: Date = new Date(req.body.deadline);
            const userId: string = (req.body.userId).toString();

            if (!id || !name || !deadline) {
                return res.status(400).json({ success: false, msg: 'Undefined' });
            }

            if (isNaN(id) || typeof id !== 'number' || typeof name !== 'string' || !(deadline instanceof Date)) {
                return res.status(400).json({ success: false, msg: 'Type Error' });
            }

            const result = await this.todoListService.createTask(id, name, deadline, description);

            if (result.success) {
                this.io.to(userId).emit("task-created");
                return res.json(result);
            } else {
                return res.status(500).json(result);
            }
        } catch (error) {
            return res.status(500).json({ success: false, msg: error.message });
        }
    }

    editTask = async (req: Request, res: Response) => {
        try {
            const id: number = parseInt(req.body.id);
            const name: string = req.body.name;
            const description: string = req.body.description;
            const deadline: Date = new Date(req.body.deadline);
            const userId: string = (req.body.userId).toString();

            if (!id || !name || !deadline) {
                return res.status(400).json({ success: false, msg: 'Undefined' });
            }

            if (isNaN(id) || typeof id !== 'number' || typeof name !== 'string' || !(deadline instanceof Date)) {
                return res.status(400).json({ success: false, msg: 'Type Error' });
            }

            const result = await this.todoListService.editTask(id, name, deadline, description);

            if (result.success) {
                this.io.to(userId).emit("task-edited");
                return res.json(result);
            } else {
                return res.status(500).json(result);
            }
        } catch (error) {
            return res.status(500).json({ success: false, msg: error.message });
        }
    }

    moveTask = async (req: Request, res: Response) => {
        try {
            const taskId = parseInt(req.body.taskId);
            const listId = parseInt(req.body.listId);
            const userId = (req.body.userId).toString();

            if (!taskId || !listId) {
                return res.status(400).json({ success: false, msg: 'Undefined' });
            }

            if (isNaN(taskId) || isNaN(listId) || typeof taskId !== 'number' || typeof listId !== 'number') {
                return res.status(400).json({ success: false, msg: 'Type Error' });
            }

            const result = await this.todoListService.moveTask(taskId, listId);

            if (result.success) {
                this.io.to(userId).emit("task-moved");
                return res.json(result);
            } else {
                return res.status(500).json(result);
            }
        } catch (error) {
            return res.status(500).json({ success: false, msg: error.message });
        }
    }

    moveTasks = async (req: Request, res: Response) => {
        try {
            const tasks = req.body;
            const userId = (req.body.userId).toString();

            if (!tasks.taskId || !tasks.listId) {
                return res.status(400).json({ success: false, msg: 'Undefined' });
            }

            for (let taskId of tasks.taskId) {
                if (isNaN(taskId) || typeof taskId !== 'number') {
                    return res.status(400).json({ success: false, msg: 'Type Error' });
                }
            }

            if (isNaN(tasks.listId) || typeof tasks.listId !== 'number') {
                return res.status(400).json({ success: false, msg: 'Type Error' });
            }

            const result = await this.todoListService.moveTasks(tasks);

            if (result.success) {
                this.io.to(userId).emit("tasks-moved");
                return res.json(result);
            } else {
                return res.status(500).json(result);
            }
        } catch (error) {
            return res.status(500).json({ success: false, msg: error.message });
        }
    }

    completeTask = async (req: Request, res: Response) => {
        try {
            const id = parseInt(req.params.id);
            const userId = (req.body.userId).toString();

            if (!id) {
                return res.status(400).json({ success: false, msg: 'Undefined' });
            }

            if (isNaN(id) || typeof id !== 'number') {
                return res.status(400).json({ success: false, msg: 'Type Error' });
            }

            const result = await this.todoListService.completeTask(id);

            if (result.success) {
                logger.info("Congrats: " + `User id ${userId} have completed task id ${id}`);
                this.io.to(userId).emit("task-completed");
                return res.json(result);
            } else {
                return res.status(500).json(result);
            }
        } catch (error) {
            return res.status(500).json({ success: false, msg: error.message });
        }
    }

    removeList = async (req: Request, res: Response) => {
        try {
            const id = parseInt(req.params.id);
            const userId = (req.body.userId).toString();

            if (!id) {
                return res.status(400).json({ success: false, msg: 'Undefined' });
            }

            if (isNaN(id) || typeof id !== 'number') {
                return res.status(400).json({ success: false, msg: 'Type Error' });
            }

            const result = await this.todoListService.removeList(id);

            if (result.success) {
                this.io.to(userId).emit("list-removed");
                return res.json(result);
            } else {
                return res.status(500).json(result);
            }
        } catch (error) {
            return res.status(500).json({ success: false, msg: error.message });
        }
    }

    removeTask = async (req: Request, res: Response) => {
        try {
            const id = parseInt(req.params.id);
            const userId = (req.body.userId).toString();

            if (!id) {
                return res.status(400).json({ success: false, msg: 'Undefined' });
            }

            if (isNaN(id) || typeof id !== 'number') {
                return res.status(400).json({ success: false, msg: 'Type Error' });
            }

            const result = await this.todoListService.removeTask(id);

            if (result.success) {
                this.io.to(userId).emit("task-removed");
                return res.json(result);
            } else {
                return res.status(500).json(result);
            }
        } catch (error) {
            return res.status(500).json({ success: false, msg: error.message });
        }
    }

    removeTasks = async (req: Request, res: Response) => {
        try {
            const { ids, userId } = req.body;

            for (let id of ids) {
                if (isNaN(id) || typeof id !== 'number') {
                    return res.status(400).json({ success: false, msg: 'Type Error' });
                }
            }

            const result = await this.todoListService.removeTasks(ids);

            if (result.success) {
                this.io.to((userId.toString())).emit("tasks-removed");
                return res.json(result);
            } else {
                return res.status(500).json(result);
            }
        } catch (error) {
            return res.status(500).json({ success: false, msg: error.message });
        }
    }
}