import { add } from "date-fns";
import { Knex } from "knex";
import { TaskStatusEnum } from "../Enums/taskStatusEnum";
import { hashPassword } from '../hash';

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("tasks").del();
    await knex("lists").del();
    await knex("users").del();

    // Inserts seed entries
    // users seed data
    interface User {
        username: string
        email: string
        password: string
    }

    let users: User[] = [];
    users.push(
        {
            username: 'admin',
            email: 'admin@admin.com',
            password: await hashPassword("admin")
        },
        {
            username: 'user',
            email: 'user@user.com',
            password: await hashPassword("user")
        },
        {
            username: 'driver',
            email: 'driver@tecky.io',
            password: await hashPassword("driver")
        },
        {
            username: 'thomas',
            email: 'thomas@tecky.io',
            password: await hashPassword("thomas")
        },
        {
            username: 'test',
            email: 'test@tecky.io',
            password: await hashPassword("test")
        }
    )

    const user_id: number[] = await knex.insert(users).into("users").returning('id');

    // list seed date
    interface List {
        user_id: number
        name: string
    }

    let lists: List[] = []
    lists.push(
        {
            user_id: user_id[0],
            name: 'Important',
        },
        {
            user_id: user_id[0],
            name: 'Testing',
        },
        {
            user_id: user_id[0],
            name: 'Hello',
        },
        {
            user_id: user_id[1],
            name: 'Important',
        },
    )
    const list_id: number[] = await knex.insert(lists).into("lists").returning('id');

    // tasks seed data
    interface Task {
        list_id: number
        name: string
        description?: string
        status: string
        deadline: Date,
    }

    let tasks: Task[] = [];
    tasks.push(
        {
            list_id: list_id[0],
            name: 'Send email',
            description: 'Send email to Thomas',
            status: TaskStatusEnum.TO_DO,
            deadline: add(new Date(), { minutes: 10 })
        },
        {
            list_id: list_id[0],
            name: 'Pack',
            status: TaskStatusEnum.TO_DO,
            deadline: add(new Date(), { minutes: 10 })
        },
        {
            list_id: list_id[0],
            name: 'Eat medicine',
            status: TaskStatusEnum.TO_DO,
            deadline: add(new Date(), { minutes: 1 })
        },
        {
            list_id: list_id[0],
            name: 'Revision',
            status: TaskStatusEnum.COMPLETED,
            deadline: new Date()
        },
        {
            list_id: list_id[1],
            name: 'Send email',
            description: 'Send email to Alex',
            status: TaskStatusEnum.TO_DO,
            deadline: add(new Date(), { minutes: 10 })
        },
        {
            list_id: list_id[1],
            name: 'Pack stuff',
            status: TaskStatusEnum.TO_DO,
            deadline: add(new Date(), { minutes: 10 })
        },
        {
            list_id: list_id[1],
            name: 'Eat dinner with Mary',
            status: TaskStatusEnum.TO_DO,
            deadline: add(new Date(), { minutes: 1 })
        },
        {
            list_id: list_id[1],
            name: 'Revise for tmr exam',
            status: TaskStatusEnum.COMPLETED,
            deadline: new Date()
        },
        {
            list_id: list_id[2],
            name: 'Send email',
            description: 'Send email to Tecky',
            status: TaskStatusEnum.TO_DO,
            deadline: add(new Date(), { minutes: 10 })
        },
        {
            list_id: list_id[2],
            name: 'Pack stuff for the trip tmr',
            status: TaskStatusEnum.TO_DO,
            deadline: add(new Date(), { minutes: 10 })
        },
        {
            list_id: list_id[2],
            name: 'Eat dinner with Jess',
            status: TaskStatusEnum.TO_DO,
            deadline: add(new Date(), { minutes: 1 })
        },
        {
            list_id: list_id[2],
            name: 'Revise for tmr quiz',
            status: TaskStatusEnum.COMPLETED,
            deadline: new Date()
        },
    )

    await knex.insert(tasks).into("tasks");
};
