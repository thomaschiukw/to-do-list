import express from "express";
import { isLoggedIn } from "../guard";
import { todoListController } from "../server";

const todoListRoutes = express.Router();

todoListRoutes.get("/lists", isLoggedIn, (req, res) => todoListController.getAllLists(req, res));
todoListRoutes.get("/lists/user/:id", isLoggedIn, (req, res) => todoListController.getListsByUser(req, res));
todoListRoutes.get("/tasks/user/:id", isLoggedIn, (req, res) => todoListController.getTasksByUser(req, res));

todoListRoutes.post("/list", isLoggedIn, (req, res) => todoListController.createList(req, res));
todoListRoutes.post("/task", isLoggedIn, (req, res) => todoListController.createTask(req, res));

todoListRoutes.put("/task/edit", isLoggedIn, (req, res) => todoListController.editTask(req, res));
todoListRoutes.put("/task/move", isLoggedIn, (req, res) => todoListController.moveTask(req, res));
todoListRoutes.put("/tasks/move", isLoggedIn, (req, res) => todoListController.moveTasks(req, res));
todoListRoutes.put("/task/complete/:id", isLoggedIn, (req, res) => todoListController.completeTask(req, res));

todoListRoutes.delete("/list/:id", isLoggedIn, (req, res) => todoListController.removeList(req, res));
todoListRoutes.delete("/task/:id", isLoggedIn, (req, res) => todoListController.removeTask(req, res));
todoListRoutes.delete("/tasks", isLoggedIn, (req, res) => todoListController.removeTasks(req, res));

export default todoListRoutes;