import express from "express";
import { authController } from "../server";

const userRoutes = express.Router();

userRoutes.post("/login", (req, res) => authController.login(req, res));
userRoutes.post("/register", (req, res) => authController.register(req, res));

export default userRoutes;