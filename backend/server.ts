import express from 'express';
import expressSession from "express-session";
import http from 'http';
import { knex } from './db';
import { Server as SocketIO } from 'socket.io';
import { env } from './env';
import { logger } from './logger';
import { setSocketIO } from './socketio';
import cors from 'cors';
import { AuthController } from './controllers/AuthController';
import { TodoListController } from './controllers/TodoListController';
import { UserService } from './services/UserService';
import { TodoListService } from './services/TodoListService';
import userRoutes from './router/userRoutes';
import todoListRoutes from './router/todoListRoutes';

export const app = express();
const server = new http.Server(app);
const io = new SocketIO(server, {
    cors: {
        origin: "http://localhost:3000",
        methods: ["GET", "POST", "PUT", "DELETE"],
    }
});
setSocketIO(io);

export const sessionMiddleware = expressSession({
    secret: 'Taxi Pool',
    resave: true,
    saveUninitialized: true,
    cookie: { secure: false }
});

app.use(cors({
    origin: ['http://localhost:3000']
}))

app.use(sessionMiddleware);
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

export const userService = new UserService(knex);
const todoListService = new TodoListService(knex);

export const authController = new AuthController(userService);
export const todoListController = new TodoListController(todoListService, io);

app.use(userRoutes);
app.use(todoListRoutes);

const PORT = env.PORT;

server.listen(PORT, () => {
    logger.info(`Listening at http://localhost:${PORT}/`);
});