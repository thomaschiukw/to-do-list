import express from "express";
import socketIO from "socket.io";
import { logger } from "./logger";
import { sessionMiddleware } from "./server";

export let io: socketIO.Server;

export function setSocketIO(value: socketIO.Server) {
    io = value;

    io.use((socket, next) => {
        let req = socket.request as express.Request
        let res = req.res as express.Response
        sessionMiddleware(req, res, next as express.NextFunction)
    });

    io.on("connection", (socket) => {
        socket.on('join', function (data) {           
            socket.join((data.id).toString());
        });
        logger.info("Client connected: " + socket.id);
    });
}