import dotenv from "dotenv";
dotenv.config();

export const env = {
    DB_NAME: process.env.DB_NAME,
    DB_USER: process.env.DB_USER,
    DB_PASSWORD: process.env.DB_PASSWORD,
    DB_HOST: process.env.DB_HOST || "localhost",
    DB_PORT: process.env.DB_PORT,
    NODE_ENV: process.env.NODE_ENV,
    PORT: +process.env.PORT! || 8080,
    JWT_SECRET: process.env.JWT_SECRET!,
    LOGGING_LEVEL: process.env.LOGGING_LEVEL || "silly",
}