import { NextFunction, Request, Response } from 'express';
import { Bearer } from 'permit';
import jwtSimple from 'jwt-simple';
import jwt from './jwt';
import { User } from './model';
import { userService } from './server';
import { logger } from './logger';

const permit = new Bearer({
    query: "access_token"
});

export const isLoggedIn = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const token = permit.check(req);
        
        if (!token) {
            return res.status(401).json({ success: false, msg: "Permission Denied" });
        }
        const payload = jwtSimple.decode(token, jwt.jwtSecret);

        const user: User = (await userService.getUserById(payload.user_id))[0];

        if (user) {
            req.user = user[0];
            return next();
        } else {
            return res.status(401).json({ success: false, msg: "Permission Denied" });
        }
    } catch (e) {
        logger.error("Guard Error: " + JSON.stringify(e.message));
        return res.status(401).json({ success: false, msg: "Permission Denied" });
    }
}