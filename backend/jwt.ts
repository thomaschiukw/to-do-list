import { env } from './env';

export default {
    jwtSecret: env.JWT_SECRET,
    jwtSession: {
        session: false
    }
}